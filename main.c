
#include "main.h"

#include "leds_control.h"

/* Globals ---------------------------------------------*/
char streaming_on = 0;

int i = 0, j = 0, k = 0, l = 0;
char cmd_buf[RX_BUF_SIZE] = {0,};
char initialized = 0;

u8 tgtRed = 255, tgtGreen = 0, tgtBlue = 0;

SS_Model redSS = {
        .A = {0.6678, -0.827, 0.04135,  0.9779},
        .B = {0.04135, 0.001103},
        .C = {0, 20},
        .D = 0,
        .X = {0, 0}
};

SS_Model greenSS = {
        .A = {0.6678, -0.827, 0.04135,  0.9779},
        .B = {0.04135, 0.001103},
        .C = {0, 20},
        .D = 0,
        .X = {0, 0}
};

SS_Model blueSS = {
        .A = {0.6678, -0.827, 0.04135,  0.9779},
        .B = {0.04135, 0.001103},
        .C = {0, 20},
        .D = 0,
        .X = {0, 0}
};

/*------------------------------------------------------*/

int main(void)
{
    SystemInit();

    /* Ensure all priority bits are assigned as preemption priority bits. */
    NVIC_PriorityGroupConfig( NVIC_PriorityGroup_4 );

    UB_Uart_Init();

    init_SPI2();

    WS2811_push(0, 0, 0);

    Delay(10000);

    WS2811_push(255, 0, 0);

    Delay(10000000);

    WS2811_push(0, 255, 0);

    Delay(10000000);

    WS2811_push(0, 0, 255);

    Delay(10000000);

    WS2811_push(0, 0, 0);

    xTaskCreate( vTask_RemoteCommandHandling, "CmdHandling", configMINIMAL_STACK_SIZE + 10, ( void * ) NULL, tskIDLE_PRIORITY + 1, NULL );

    xTaskCreate( vTask_UpdateRGB, "UpdRGB", configMINIMAL_STACK_SIZE + 10, ( void * ) NULL, tskIDLE_PRIORITY + 2, NULL );

    /* Start the scheduler. */
    vTaskStartScheduler();

    /* If all is well, the scheduler will now be running, and the following line
    will never be reached.  If the following line does execute, then there was
    insufficient FreeRTOS heap memory available for the idle and/or timer tasks
    to be created.  See the memory management section on the FreeRTOS web site
    for more details. */
    for(;;);

}

static double SS_step(SS_Model *ss, double tgt){
    double xn1 = ss->A[0] * ss->X[0] + ss->A[1] * ss->X[1] + ss->B[0] * tgt;
    double xn2 = ss->A[2] * ss->X[0] + ss->A[3] * ss->X[1] + ss->B[1] * tgt;

    ss->X[0] = xn1;
    ss->X[1] = xn2;

    return ss->C[0] * ss->X[0] + ss->C[1] * ss->X[1] + ss->D * tgt;
}

void vTask_UpdateRGB(void *pvParams)
{
    TickType_t xLastWakeTime;
    const TickType_t xFrequency = 50 / portTICK_RATE_MS;

    // Initialize the xLastWakeTime variable with the current time.
    xLastWakeTime = xTaskGetTickCount();

    for( ;; )
    {
         // Wait for the next cycle.
         vTaskDelayUntil( &xLastWakeTime, xFrequency );

         // Perform action here.
         u8 red = (u8)COERCE_DOUBLE(0, SS_step(&redSS, tgtRed), 255);
         u8 green = (u8)COERCE_DOUBLE(0, SS_step(&greenSS, tgtGreen), 255);
         u8 blue = (u8)COERCE_DOUBLE(0, SS_step(&blueSS, tgtBlue), 255);

         WS2811_push(red, green, blue);
    }
}

void vTask_RemoteCommandHandling(void *pvParams)
{
    for(;;){
        UART_RXSTATUS_t rcv_status;
        u16 cmd;

        rcv_status = UB_Uart_ReceiveString(COM2, cmd_buf);

        if (rcv_status == RX_READY) {
            cmd = (cmd_buf[0] << 8) + cmd_buf[1];
            Parse_RemoteCommand(cmd, cmd_buf);
        }
    }
}

void Parse_RemoteCommand(u16 cmd, char *buf)
{
    if (cmd == CMD_STREAM_STATE) {
        if (buf[2] == CMD_OPT_ON) {
            streaming_on = 1;
        }
        else if (buf[2] == CMD_OPT_OFF) {
            streaming_on = 0;
        }
    }
    else if (cmd == CMD_LED_R) {
        switch_led_red(buf[2]);
    }
    else if (cmd == CMD_LED_G) {
        switch_led_green(buf[2]);
    }
    else if (cmd == CMD_LED_B) {
        switch_led_blue(buf[2]);
    }
    else if (cmd == CMD_PWM_RGB) {
        char *pwm_str = &buf[2];
        char * ptr_end;

        tgtRed = (u8)COERCE_DOUBLE(0, strtol(pwm_str,&ptr_end,10), 255);
        tgtGreen = (u8)COERCE_DOUBLE(0, strtol (ptr_end,&ptr_end,10), 255);
        tgtBlue = (u8)COERCE_DOUBLE(0, strtol (ptr_end,NULL,10), 255);

        //WS2811_push(red & 0xFF, green & 0xFF, blue & 0xFF);
    }
}

void Gpio_Init(void)
{

//    GPIO_InitTypeDef  GPIO_InitStructure;

    /* GPIOD Periph clock enable */
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
    //RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
    //RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE) ;     // TIM4 Periph clock enable

    GPIO_PinAFConfig( GPIOB, GPIO_PinSource7, GPIO_AF_TIM4 );
    GPIO_PinAFConfig( GPIOB, GPIO_PinSource8, GPIO_AF_TIM4 );
    GPIO_PinAFConfig( GPIOB, GPIO_PinSource9, GPIO_AF_TIM4 );

    Initialize_Leds_GPIO(LEDRGB_Out);

    // Initialize TIM4
    /*
        Set timer period when it have reset
        First you have to know max value for timer
        In our case it is 16bit = 65535
        To get your frequency for PWM, equation is simple

        PWM_frequency = timer_tick_frequency / (TIM_Period + 1)

        If you know your PWM frequency you want to have timer period set correct

        TIM_Period = timer_tick_frequency / PWM_frequency - 1

        In our case, for 10Khz PWM_frequency, set Period to

        TIM_Period = 84000000 / 10000 - 1 = 8399

        If you get TIM_Period larger than max timer value (in our case 65535),
        you have to choose larger prescaler and slow down timer tick frequency
    */
    Initialize_Leds_Timer ( TIM4, LED_PWM_Prescaler, LED_PWM_Period-1 );

    // Initialize PWMs 2-4
    Initialize_Leds_PWM ( TIM4 );
}

void Delay(__IO uint32_t nCount)
{
  while(nCount--)
  {
  }
}

/*-----------------------------------------------------------*/
void vApplicationTickHook( void )
{
}

void vApplicationMallocFailedHook( void )
{
    /* vApplicationMallocFailedHook() will only be called if
    configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h.  It is a hook
    function that will get called if a call to pvPortMalloc() fails.
    pvPortMalloc() is called internally by the kernel whenever a task, queue,
    timer or semaphore is created.  It is also called by various parts of the
    demo application.  If heap_1.c or heap_2.c are used, then the size of the
    heap available to pvPortMalloc() is defined by configTOTAL_HEAP_SIZE in
    FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can be used
    to query the size of free heap space that remains (although it does not
    provide information on how the remaining heap might be fragmented). */
    taskDISABLE_INTERRUPTS();
    for( ;; );
}

void vApplicationIdleHook( void )
{
    /* vApplicationIdleHook() will only be called if configUSE_IDLE_HOOK is set
    to 1 in FreeRTOSConfig.h.  It will be called on each iteration of the idle
    task.  It is essential that code added to this hook function never attempts
    to block in any way (for example, call xQueueReceive() with a block time
    specified, or call vTaskDelay()).  If the application makes use of the
    vTaskDelete() API function (as this demo application does) then it is also
    important that vApplicationIdleHook() is permitted to return to its calling
    function, because it is the responsibility of the idle task to clean up
    memory allocated by the kernel to any task that has since been deleted. */
}

void vApplicationStackOverflowHook( TaskHandle_t pxTask, char *pcTaskName )
{
    ( void ) pcTaskName;
    ( void ) pxTask;

    /* Run time stack overflow checking is performed if
    configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2.  This hook
    function is called if a stack overflow is detected. */
    taskDISABLE_INTERRUPTS();
    for( ;; );
}
/*-----------------------------------------------------------*/
