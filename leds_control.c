#include "main.h"

#include "leds_control.h"

// GLOBALS
char active_led = 0;

TIM_OCInitTypeDef TIM_OCStruct = {
        /* PWM mode 2 = Clear on compare match */
        /* PWM mode 1 = Set on compare match */
        .TIM_OCMode = TIM_OCMode_PWM2,
        .TIM_OutputState = TIM_OutputState_Enable,
        .TIM_OCPolarity = TIM_OCPolarity_Low
};

/// globals

void switch_led_red(REMOTE_CMD_OPT_t opt)
{
    if (opt == CMD_OPT_ON) {
        LEDRed_PWM_set(5000);
        LEDGreen_PWM_set(0);
        LEDBlue_PWM_set(0);

        active_led = LEDRed_Active;
    }
    else if (opt == CMD_OPT_OFF) {
        LEDRed_PWM_set(0);

        if (active_led == LEDRed_Active) {
            active_led = 0;
        }
    }
}

void switch_led_green(REMOTE_CMD_OPT_t opt)
{
    if (opt == CMD_OPT_ON) {
        LEDRed_PWM_set(0);
        LEDGreen_PWM_set(5000);
        LEDBlue_PWM_set(0);


        active_led = LEDGreen_Active;
    }
    else if (opt == CMD_OPT_OFF) {
//        GPIO_ResetBits(LEDGreen_Out);
        LEDGreen_PWM_set(0);

        if (active_led == LEDGreen_Active) {
            active_led = 0;
        }
    }
}

void switch_led_blue(REMOTE_CMD_OPT_t opt)
{
    if (opt == CMD_OPT_ON) {
        LEDRed_PWM_set(0);
        LEDGreen_PWM_set(0);
        LEDBlue_PWM_set(5000);

        active_led = LEDBlue_Active;
    }
    else if (opt == CMD_OPT_OFF) {
        LEDBlue_PWM_set(0);

        if (active_led == LEDBlue_Active) {
            active_led = 0;
        }
    }
}


/**
  * @brief  Setup the GPIOx port as out of PWM on selected Pin.
  * @param  GPIOx: where x can be (A..I) to select the GPIO peripheral.
  * @param  Pin: specifies the port bit to be initialized for out of PWM signal.
  *             Refer STM32F407xx Datasheet for Alternate function mapping (Table 9)
  * @retval None
  */
void Initialize_Leds_GPIO ( GPIO_TypeDef* GPIOx, u16 Pin )
{
    GPIO_InitTypeDef sg;

    sg.GPIO_Pin   = Pin;
    sg.GPIO_Mode  = GPIO_Mode_AF;       // PWM is Alternate function Mode
    sg.GPIO_Speed = GPIO_Speed_100MHz;
    sg.GPIO_OType = GPIO_OType_PP;
    sg.GPIO_PuPd  = GPIO_PuPd_UP;

    GPIO_Init( GPIOx, &sg );
}


/**
  * @brief  Initialize timer TIMx as counter with the specified Period and Prescaler.
  * @param  TIMx: where x can be  1 to 14 to select the TIM peripheral.
  * @param  Period: the length of period will be n+1 because the counting begin from 0
  * @param  PrescaleDiv: Divides the clock at PrescaleDiv+1.
  *             If PrescaleDiv=0 then SrcClock not changed.
  * @retval None
  */
void Initialize_Leds_Timer ( TIM_TypeDef* TIMx, u16 PrescaleDiv, u16 Period )
{
    TIM_TimeBaseInitTypeDef sT;

    sT.TIM_Prescaler   = PrescaleDiv;
    sT.TIM_CounterMode = TIM_CounterMode_Up;// Count to up
    sT.TIM_Period      = Period;
    sT.TIM_ClockDivision = TIM_CKD_DIV1;    // No pre-division
    sT.TIM_RepetitionCounter = 0;

    TIM_TimeBaseInit( TIMx, &sT );

    TIM_Cmd( TIMx, ENABLE );
}

/**
  * @brief  Initialize PWM channel 4 of TIMx
  * @param  TIMx: where x can be  1 to 14 to select the TIM peripheral.
  * @param  ValueToOn: the value of TIMx starting from which PWM out will be 1
  * @retval None
  */
void Initialize_Leds_PWM ( TIM_TypeDef* TIMx )
{
    /*
        To get proper duty cycle, you have simple equation

        pulse_length = ((TIM_Period + 1) * DutyCycle) / 100 - 1

        where DutyCycle is in percent, between 0 and 100%

        25% duty cycle:     pulse_length = ((8399 + 1) * 25) / 100 - 1 = 2099
        50% duty cycle:     pulse_length = ((8399 + 1) * 50) / 100 - 1 = 4199
        75% duty cycle:     pulse_length = ((8399 + 1) * 75) / 100 - 1 = 6299
        100% duty cycle:    pulse_length = ((8399 + 1) * 100) / 100 - 1 = 8399

        Remember: if pulse_length is larger than TIM_Period, you will have output HIGH all the time
    */
//        TIM_OCStruct.TIM_Pulse = 2100-1; /* 25% duty cycle */
//        TIM_OC1Init(TIM4, &TIM_OCStruct);
//        TIM_OC1PreloadConfig(TIM4, TIM_OCPreload_Enable);

        LEDRed_PWM_set(0);

        LEDGreen_PWM_set(0);

        LEDBlue_PWM_set(0);
}

void LEDRed_PWM_set(u32 pulse)
{
    // OC2 => PB7 => Red
    TIM_OCStruct.TIM_Pulse = pulse;
    TIM_OC2Init(TIM4, &TIM_OCStruct);
    TIM_OC2PreloadConfig(TIM4, TIM_OCPreload_Enable);
}
void LEDGreen_PWM_set(u32 pulse)
{
    // OC3 => PB7 => Red
    TIM_OCStruct.TIM_Pulse = pulse;
    TIM_OC3Init(TIM4, &TIM_OCStruct);
    TIM_OC3PreloadConfig(TIM4, TIM_OCPreload_Enable);
}
void LEDBlue_PWM_set(u32 pulse)
{
    // OC4 => PB7 => Red
    TIM_OCStruct.TIM_Pulse = pulse;
    TIM_OC4Init(TIM4, &TIM_OCStruct);
    TIM_OC4PreloadConfig(TIM4, TIM_OCPreload_Enable);
}


void WS2811_push(u8 red, u8 green, u8 blue)
{
    u8 i;

    u8 a_red[4] = WS2811_bytes(red);

    u8 a_green[4] = WS2811_bytes(green);

    u8 a_blue[4] = WS2811_bytes(blue);

    // 4 SPI bytes to emulate 1 WS2811 byte. Each WS2811 bit coded using 4 SPI bits.
    // WS: 0 => SPI: 1000
    // WS: 1 => SPI: 1100
    for (i = 0; i < 4; ++i) {
        SPI2_send(a_red[0]); SPI2_send(a_red[1]); SPI2_send(a_red[2]); SPI2_send(a_red[3]);

        SPI2_send(a_green[0]); SPI2_send(a_green[1]); SPI2_send(a_green[2]); SPI2_send(a_green[3]);

        SPI2_send(a_blue[0]); SPI2_send(a_blue[1]); SPI2_send(a_blue[2]); SPI2_send(a_blue[3]);
    }
}
