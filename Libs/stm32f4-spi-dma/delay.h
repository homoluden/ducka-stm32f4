
/*
 * File:   HCMS3907.h
 * Author: peterharrison
 *
 * Created on 10 March 2012, 22:42
 */

#ifndef DELAY_H
#define	DELAY_H

#ifdef	__cplusplus
extern "C" {
#endif

  void delay_us (uint32_t microseconds);
  void delay_ms (uint32_t milliseconds);

#ifdef	__cplusplus
}
#endif

#endif