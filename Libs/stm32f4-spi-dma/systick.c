#include "stm32f4xx.h"
#include "systick.h"
#include "discovery-hardware.h"


static volatile uint32_t ticks;
static RCC_ClocksTypeDef RCC_Clocks;


static action_t action[32];
static uint16_t actionCount = 0;

void addTimerAction (action_t f)
{
  if (actionCount < 32) {
    action[actionCount++] = f;
  }
}

void systickInit (uint16_t frequency)
{
  RCC_GetClocksFreq (&RCC_Clocks);
  SysTick_Config (RCC_Clocks.HCLK_Frequency / frequency);
}


// return the system clock as milliseconds
uint32_t millis (void)
{
  return ticks;
}

void SysTick_Handler (void)
{
  uint8_t i;
  STATUS_OFF();
  ticks++;
  for (i = 0; i < actionCount; i++) {
    action[i]();
  }
  STATUS_ON();
}