
/*
 * File:   HCMS3907.c
 * Author: peterharrison
 *
 * Created on 10 March 2012, 22:42
 *
 * The HCMS-29xx series are high performance, easy to use dot matrix displays
 * driven by on-board CMOS ICs. Each display can be directly interfaced with
 * a microprocessor, thus eliminating the need for cumbersome interface
 * components. The serial IC interface allows higher character count
 * information displays with a minimum of data lines. A variety of colours,
 * font heights, and character counts gives designers a wide range of product
 * choices for their specific applications and the easy to read 5 x 7 pixel
 * format allows the display of uppercase, lower case, Katakana, and custom
 * user- defined characters. These displays are stackable in the x- and y-
 * directions, making them ideal for high character count displays.
 *
 * The HCMS3907 is a 3.3V green device with four characters.
 *
 * It is a write-only peripheral driven over SPI. As bytes are clocked in, the
 * right-most column of LEDs lights up with the most significant 7 bits and
 * all the columns shift left one place. Data in the shift registers is
 * transferred to the display when CS goes high so that the entire display
 * can be updated in one go.
 *
 * A register select line allows commands to be sent to set brightness and such.
 *
 * See the full product information at
 *
 * http://www.avagotech.com/pages/en/led_displays/smart_alphanumeric_displays/serial_interface/
 *
 *
 */

#include <stdint.h>
#include <stdbool.h>
#include <assert.h>
#include "stm32f4xx.h"
#include "discovery-hardware.h"
#include "font5x7.h"
#include "HCMS3907.h"
#include "systick.h"

// This buffer holds raw data for the display
// normally, text is rendered into it and only the first 20 bytes
// (for a four character display) are sent out - generally by DMA.
// Since the display is latched, it only need to get sent once.
// more than 20 bytes are used to make it easier to scroll the display if needed
#define HCMS3907_BUFFER_LENGTH 32
char HCMS3907_buffer[HCMS3907_BUFFER_LENGTH];

// DMA transfers can be made from anywhere so a pointer is used. It normally
// ends up pointing to the buffer though.
static uint8_t *HCMS3907_SrcAddress;
uint8_t HCMS3907_refresh_count; //tells the DMA updater to refresh the display
static int16_t HCMS3907_blinkTime;
static int16_t HCMS3907_blinkDelay;
static uint8_t HCMS3907_brightness = 6;
static uint8_t HCMS3907_enabled = 1;
static bool spiConfigured = false;

// wait as long as the DMA attached to the display is in the state given
void HCMS3907_WaitWhileDMA (FunctionalState state)
{
  while (DMA_GetCmdStatus (SPI_PORT_TX_DMA_STREAM) == state) {
  }
}

void HardwareSPI_init (void)
{
  DMA_InitTypeDef DMA_InitStructure;
  SPI_InitTypeDef SPI_InitStructure;
  NVIC_InitTypeDef NVIC_InitStructure;
  GPIO_InitTypeDef GPIO_InitStructure;
  // enable the SPI peripheral clock
  SPI_PORT_CLOCK_INIT (SPI_PORT_CLOCK, ENABLE);
  // enable the peripheral GPIO port clocks
  RCC_AHB1PeriphClockCmd (SPI_SCK_GPIO_CLK | SPI_MOSI_GPIO_CLK, ENABLE);
  // Connect SPI pins to AF5 - see section 3, Table 6 in the device data sheet
  GPIO_PinAFConfig (SPI_SCK_GPIO_PORT, SPI_SCK_SOURCE, SPI_SCK_AF);
  GPIO_PinAFConfig (SPI_MOSI_GPIO_PORT, SPI_MOSI_SOURCE, SPI_MOSI_AF);
  // now configure the pins themselves
  // they are all going to be fast push-pull outputs
  // but the SPI pins use the alternate function
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Pin = SPI_SCK_PIN;
  GPIO_Init (SPI_SCK_GPIO_PORT, &GPIO_InitStructure);
  GPIO_InitStructure.GPIO_Pin = SPI_MOSI_PIN;
  GPIO_Init (SPI_MOSI_GPIO_PORT, &GPIO_InitStructure);
  // now we can set up the SPI peripheral
  // Assume the target is write only and we look after the chip select ourselves
  // SPI clock rate will be system frequency/4/prescaler
  // so here we will go for 72/4/8 = 2.25MHz
  SPI_I2S_DeInit (SPI_PORT);
  SPI_StructInit (&SPI_InitStructure);
  SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
  SPI_InitStructure.SPI_Direction = SPI_Direction_1Line_Tx;
  SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
  SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8;
  SPI_Init (SPI_PORT, &SPI_InitStructure);
  // Enable the SPI port
  SPI_Cmd (SPI_PORT, ENABLE);
  // now set up the DMA
  // first enable the clock
  RCC_AHB1PeriphClockCmd (SPI_PORT_DMAx_CLK, ENABLE);
  // start with a blank DMA configuration just to be sure
  DMA_DeInit (SPI_PORT_TX_DMA_STREAM);
  /*
   * Check if the DMA Stream is disabled before enabling it.
   * Note that this step is useful when the same Stream is used multiple times:
   * enabled, then disabled then re-enabled... In this case, the DMA Stream disable
   * will be effective only at the end of the ongoing data transfer and it will
   * not be possible to re-configure it before making sure that the Enable bit
   * has been cleared by hardware. If the Stream is used only once, this step might
   * be bypassed.
   */
  while (DMA_GetCmdStatus (SPI_PORT_TX_DMA_STREAM) != DISABLE);
  // Configure DMA controller to manage TX DMA requests for the HCMS display
  // first make sure we are using the default values
  DMA_StructInit (&DMA_InitStructure);
  // these are the only parameters that change from the defaults
  DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) & (SPI_PORT->DR);
  DMA_InitStructure.DMA_Channel = SPI_PORT_TX_DMA_CHANNEL;
  DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
  /*
   * It is not possible to call DMA_Init without values for the source
   * address and non-zero size even though a transfer is not done here.
   * These are checked only when the assert_param macro is active though.
   */
  DMA_InitStructure.DMA_Memory0BaseAddr = 0;
  DMA_InitStructure.DMA_BufferSize = 1;
  DMA_Init (SPI_PORT_TX_DMA_STREAM, &DMA_InitStructure);
  // Enable the DMA transfer complete interrupt
  DMA_ITConfig (SPI_PORT_TX_DMA_STREAM, DMA_IT_TC, ENABLE);
  // enable the interrupt in the NVIC
  NVIC_InitStructure.NVIC_IRQChannel = SPI_PORT_DMA_TX_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init (&NVIC_InitStructure);
  // Enable dma tx request.
  SPI_I2S_DMACmd (SPI_PORT, SPI_I2S_DMAReq_Tx, ENABLE);
  spiConfigured = true;
}


void SPI_PORT_DMA_TX_IRQHandler()
{
  // Test if DMA Stream Transfer Complete interrupt
  if (DMA_GetITStatus (SPI_PORT_TX_DMA_STREAM, DMA_IT_TCIF4)) {
    DMA_ClearITPendingBit (SPI_PORT_TX_DMA_STREAM, DMA_IT_TCIF4);
    /*
     * There is an unpleasant wait until we are certain the data has been sent.
     * The need for this has been verified by oscilloscope. The shift register
     * at this point may still be clocking out data and it is not safe to
     * release the chip select line until it has finished. It only costs half
     * a microsecond so better safe than sorry. Is it...
     *
     *  a) flushed from the transmit buffer
     */
    while (SPI_I2S_GetFlagStatus (SPI_PORT, SPI_I2S_FLAG_TXE) == RESET) {
    };
    /*
     * b) flushed out of the shift register
     */
    while (SPI_I2S_GetFlagStatus (SPI_PORT, SPI_I2S_FLAG_BSY) == SET) {
    };
    /*
     * The DMA stream is disabled in hardware at the end of the transfer
     * Now we can deselect the display. If more than one peripheral was being run
     * on this SPI peripheral, we would have to do both/all of them, or work out
     * which one was active and deselect that one.
     */
    HCMS3907_DESELECT();
  }
}




/*
 * Initialising the display requires configuration of the IO port, SPI and
 * DMA hardware.
 * All the register and hardware dependencies should be defined in the system
 * header file to make it easier to set up with different pin assignments.
 */
void HCMS3907_init()
{
  GPIO_InitTypeDef GPIO_InitStructure;
  // There seems little to be gained by initialising the SPI port more than once
  if (!spiConfigured) {
    HardwareSPI_init();
  }
  // enable the peripheral GPIO port clocks
  RCC_AHB1PeriphClockCmd (HCMS3907_CS_GPIO_CLK | HCMS3907_RS_GPIO_CLK, ENABLE);
  // configure the CS and RS pins
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Pin = HCMS3907_CS_PIN;
  GPIO_Init (HCMS3907_CS_GPIO_PORT, &GPIO_InitStructure);
  GPIO_InitStructure.GPIO_Pin = HCMS3907_RS_PIN;
  GPIO_Init (HCMS3907_RS_GPIO_PORT, &GPIO_InitStructure);
  addTimerAction (HCMS3907_update);
  HCMS3907_DESELECT();        // to be on the safe side
  HCMS3907_blink (0);         // and not flashing
  HCMS3907_refresh_count = 0; // give the DMA nothing to do
  HCMS3907_off();             // fire it up
  HCMS3907_cls();             // and leave it blank
  HCMS3907_puts ("-D4-");
  // now we need to wait to ensure the string is sent since it
  // is triggered by a timer tick. This is not too elegant.
  delay_ms (2);
  // Give it a little fade in
  for (int i = 0; i < 7; i++) {
    HCMS3907_setBrightness (i);
    delay_ms (50);
  }
}


/*
 * Send a single byte of data to the display over SPI
 * Best used for commands or the updating of a single column since
 * the display latches data to the LEDs when the Chip Enable goes high
 * Polling is used always for this.
 * TODO: check that there is not a DMA transfer in progress
 */
void HCMS3907_putbyte (uint8_t c)
{
  HCMS3907_SELECT();
  // make sure the transmit buffer is free
  while (SPI_I2S_GetFlagStatus (SPI_PORT, SPI_I2S_FLAG_TXE) == RESET);
  SPI_I2S_SendData (SPI_PORT, c);
  // we are not reading data so be sure that the character goes to the shift register
  while (SPI_I2S_GetFlagStatus (SPI_PORT, SPI_I2S_FLAG_TXE) == RESET);
  // and then be sure it has been sent over the wire
  while (SPI_I2S_GetFlagStatus (SPI_PORT, SPI_I2S_FLAG_BSY) == SET);
  HCMS3907_DESELECT();
}

/*
 * A generic way to send data to the display. the details ere can be changed to
 * use a different method if desired. As listed, the code simply prepares the
 * variables used by the periodic update function to allow it to fire off
 * a DMA transfer.
 * IF desired, the DMA transfer could be called directly here as could the
 * polled transfer
 */
void HCMS3907_putBuffer (uint8_t *buf, uint8_t len)
{
  assert_param (len <= HCMS3907_BUFFER_LENGTH);
  HCMS3907_SrcAddress = buf;
  HCMS3907_refresh_count = len;
}

/*
 * A simple polled transfer over the SPI port. This is a blocking function which
 * will only return when the transfer is complete. It is assumed that only data bytes get
 * sent using this call as all the commands are one or two bytes most conveniently
 * handled individually.
 */
void HCMS3907_putBufferPolled (uint8_t *buf, uint8_t len)
{
  assert_param (len <= HCMS3907_BUFFER_LENGTH);
  HCMS3907_DATA();
  HCMS3907_SELECT();
  while (len) {
    // always make sure the transmit buffer is free
    while (SPI_I2S_GetFlagStatus (SPI_PORT, SPI_I2S_FLAG_TXE) == RESET);
    SPI_I2S_SendData (SPI_PORT, *buf++);
    len--;
  }
  // be sure the last byte is sent to the shift register
  while (SPI_I2S_GetFlagStatus (SPI_PORT, SPI_I2S_FLAG_TXE) == RESET);
  // and then wait until it goes over the wire
  while (SPI_I2S_GetFlagStatus (SPI_PORT, SPI_I2S_FLAG_BSY) == SET);
  HCMS3907_DESELECT();
}

/*
 * This transfer uses the DMA peripheral. Again, only data is assumed to be sent
 * Once the DMA peripheral registers are set up, the stream is enabled and everything
 * will get sent automatically.
 * At the end of the transfer, an interrupt is generated where the transfer can be tidied
 * up and the display deselected.
 * Note that both the source address and the length are 32 bit values so you can transfer plenty of data
 */
void HCMS3907_putBufferDMA (uint8_t *buf, uint8_t len)
{
  assert_param (len <= HCMS3907_BUFFER_LENGTH);
  HCMS3907_DATA();
  HCMS3907_SELECT();
  SPI_PORT_TX_DMA_STREAM->NDTR = (uint32_t) len;
  SPI_PORT_TX_DMA_STREAM->M0AR = (uint32_t) buf;
  DMA_Cmd (SPI_PORT_TX_DMA_STREAM, ENABLE);
}

/*
 * send a command byte to the display. Leave the chip in data mode.
 */
void HCMS3907_cmd (uint8_t c)
{
  HCMS3907_COMMAND();
  HCMS3907_putbyte (c);
  HCMS3907_DATA();
}

/*
 * send a data byte to the display. Leave the chip in data mode.
 *
 */
void HCMS3907_data (uint8_t c)
{
  HCMS3907_DATA();
  HCMS3907_putbyte (c);
}

/*
 * Send five more columns to the display with data from a
 * single character. Whenever new data arrives in the display, it clocks
 * old data out the end so the display shifts left;
 */
void HCMS3907_putchar (const char c)
{
  char *fontData;
  fontData = (char *) Font5x7 + (c % 128) * 5;
  HCMS3907_putBuffer (fontData, 5);
}

/*
 * A string of more or less arbitrary length is converted to a bitmap in the
 * buffer and sent to the display. The number of bytes sent is
 * limited to the size of the buffer so cascaded displays will work. If the
 * string is longer than the display size, the last n characters will be
 * displayed, where n is the display width.
 */
void HCMS3907_puts (const char *s)
{
  int i;
  const char *fontData;
  char *buf;
  uint16_t numBytes = 0;
  buf = HCMS3907_buffer;
  while ( (*s) && (numBytes < (uint16_t) sizeof (HCMS3907_buffer) - 5)) {
    fontData = Font5x7 + (*s % 128) * 5;
    for (i = 0; i < 5; i++) {
      *buf++ = *fontData++;
    }
    numBytes += 5;
    s++;
  }
  HCMS3907_putBuffer (HCMS3907_buffer, numBytes);
}

/*
 * blanks the display, turning off the LEDs. The display data is retained
 */
void HCMS3907_off (void)
{
  HCMS3907_enabled = 0;
  HCMS3907_cmd (0);
}

/*
 * Restores the display by turning on the LEDs
 */
void HCMS3907_on (void)
{
  HCMS3907_enabled = 1;
  HCMS3907_cmd (HCMS3907_brightness | 0b01000000);
}

/*
 * Toggles display state between on and off
 */
void HCMS3907_toggle (void)
{
  if (HCMS3907_enabled) {
    HCMS3907_off();
  } else {
    HCMS3907_on();
  }
}

/*
 * Set the blink period in milliseconds. The HCMS3907_update() function, called from
 * the system timer decrements the counter and toggles the display every time
 * the counter reaches zero. Thus the display is made to blink.
 * A period of zero stops the blinking and leaves the display on all the time.
 * Small numbers for the period give more rapid blinking.
 */
void HCMS3907_blink (uint16_t period)
{
  assert_param (period <= 2000);  // don't want it so slow we think it is broken.
  HCMS3907_blinkTime = period;
}

/*
 * A control register in the display can be used to set the brightness by
 * pulse-width-modulating the LEDs. Brightness values are in the range 0-15
 */
void HCMS3907_setBrightness (uint8_t brightness)
{
  if (brightness > 15) {
    brightness = 15;
  }
  HCMS3907_brightness = brightness;
  HCMS3907_on();
}

/*
 * This function needs to be called regularly. The system timer will do the
 * job very nicely. A 1kHz rate means that the counter values are in ms.
 * Two tasks are performed. First, the blink timer is tested and the blink
 * behaviour determined.
 * Second, the number of bytes in the DMA transfer buffer is tested and, if
 * non-zero, a DMA transfer of that number of bytes is initiated.
 * On completion, an interrupt is triggered which will wait until the
 * last byte goes from the SPI hardware and then deselect the display.
 * Once sent, the display latches all the data and need not be updated again
 * unless there is a change.
 * The assumption here is that the DMA transfer will be complete within one millisecond
 * or, at least, done by the time another one is requested.
 */
void HCMS3907_update (void)
{
  if (HCMS3907_blinkTime) {
    HCMS3907_blinkDelay--;
    if (HCMS3907_blinkDelay <= 0) {
      HCMS3907_toggle();
      HCMS3907_blinkDelay = HCMS3907_blinkTime;
    }
  }
  if (HCMS3907_refresh_count) {
    HCMS3907_SELECT();
    HCMS3907_putBufferDMA (HCMS3907_SrcAddress, HCMS3907_refresh_count);
    HCMS3907_refresh_count = 0;
  }
}

/*
 * Clearing the display is conveniently done by sending null values
 * to it. The entire display buffer is emptied as well and DMA used
 * to transfer the data to avoid blocking.
 */
void HCMS3907_cls (void)
{
  uint16_t i;
  for (i = 0; i < sizeof (HCMS3907_buffer); i++) {
    HCMS3907_buffer[i] = 0;
  }
  HCMS3907_putBuffer (HCMS3907_buffer, 20);
}




