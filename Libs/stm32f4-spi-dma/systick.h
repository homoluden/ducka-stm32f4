#ifndef SYSTICK_H
#define SYSTICK_H

#ifdef __cplusplus
extern "C" {
#endif

  typedef void (*action_t) (void) ;

  void addTimerAction (action_t f);

  void systickInit (uint16_t frequency);

  uint32_t millis (void);

#ifdef __cplusplus
} // extern "C"
#endif

#endif

