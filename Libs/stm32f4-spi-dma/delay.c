/*
 * File:   delay.c
 * Author: peterharrison
 *
 * Created on 10 March 2012, 22:42
 */

#include "stm32f4xx.h"
#include "systick.h"

// TODO: nasty constant for timing. This value correct for STM32F4 at 72MHz
#define COUNTS_PER_MICROSECOND 22

inline void delay_us (uint32_t microseconds)
{
  uint32_t count = microseconds * COUNTS_PER_MICROSECOND - 2;
  __ASM volatile (
    "   mov r0, %[count]          \n\t"
    "1: subs r0, #1            \n\t"
    "   bhi 1b                 \n\t"
  :
  : [count] "r" (count)
      : "r0"
    );
}


void delay_ms (uint32_t milliseconds)
{
  uint32_t start, end;
  start = millis();
  end = start + milliseconds;
  if (start < end) {
    while ( (millis() >= start) && (millis() < end)) {
      // wait
    }
  } else {
    while ( (millis() >= start) || (millis() < end)) {
      // wait
    };
  }
}
