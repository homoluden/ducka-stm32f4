
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>
#include <string.h>
#include <cross_studio_io.h>
#include "discovery-hardware.h"
#include "systick.h"
#include "delay.h"
#include "HCMS3907.h"



/* Private typedef -----------------------------------------------------------*/
GPIO_InitTypeDef  GPIO_InitStructure;

char message[] = "    Many's the long night I've dreamed of cheese -- toasted mostly.    ";


int main (void)
{
  uint8_t i;
  RCC_AHB1PeriphClockCmd (STATUS_LEDS_PORT_CLOCK, ENABLE);
  GPIO_InitStructure.GPIO_Pin = GREEN_LED_PIN | ORANGE_LED_PIN | RED_LED_PIN | BLUE_LED_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init (GPIOD, &GPIO_InitStructure);
  systickInit (1000);
  HCMS3907_init();
  delay_ms (1000);
  while (1) {
    BLUE_ON();
    delay_ms (150);
    BLUE_OFF();
    HCMS3907_putchar (message[i]);
    i++;
    if (i >= sizeof (message)) {
      i = 0;
    }
  }
}


#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed (uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* Infinite loop */
  while (1) {
  }
}

#endif

void __assert (const char *__expression, const char *__filename, int __line)
{
  debug_printf ("ASSERT Failure: (%s) in \"%s\" on line %d\n", __expression, __filename, __line);
  debug_exit (0);
}
