/*
 * File:   HCMS3907.h
 * Author: peterharrison
 *
 * Created on 10 March 2012, 22:42
 */

#ifndef HCMS3907_H
#define	HCMS3907_H

#ifdef	__cplusplus
extern "C" {
#endif

  extern char HCMS3907_buffer[];
  extern uint8_t HCMS3907_refresh_count; //tells the DMA updater to refresh the display

  void HCMS3907_init (void);
// void HCMS3907_setMethod (SPI_TransferType method);
// SPI_TransferType HCMS3907_getMethod (SPI_TransferType method);
  void HCMS3907_cmd (uint8_t c);
  void HCMS3907_data (uint8_t c);
  void HCMS3907_putBuffer (uint8_t *buf, uint8_t len);
  void HCMS3907_putBufferPolled (uint8_t *buf, uint8_t len);
  void HCMS3907_putBufferDMA (uint8_t *buf, uint8_t len);
  void HCMS3907_putchar (const char c);
  void HCMS3907_puts (const char *s);
  void HCMS3907_on (void);
  void HCMS3907_off (void);
  void HCMS3907_toggle (void);
  void HCMS3907_update (void);
  void HCMS3907_blink (uint16_t period);
  void HCMS3907_setBrightness (uint8_t brightness);
  void HCMS3907_cls (void);

#ifdef	__cplusplus
}
#endif

#endif	/* HCMS3907_H */

