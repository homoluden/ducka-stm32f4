# README #

This repo contains the firmware sources for Ducka build signalizer (currently  it is the main purpose of Ducka). We have a plan to add more features like sound generation (synth) and animations, RFID user identification and so on.

### How do I get set up? ###

* Buy STM32F407 Discvery board
* Install ARM GCC toolchain
* Install ST-LINK/v2 driver
* Install CooCox CoIDE

Yeah.. currently only Windows supported. Maybe Linux + Wine will make it possible to work. I didn't check.