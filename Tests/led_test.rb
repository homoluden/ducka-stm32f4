require 'socket'
require 'pp'

SKT = UDPSocket.new
IP = '192.168.0.106'

class Fixnum

	def to_255
		(self / 8400.0 * 255).round
	end

end

def red
	SKT.send([34,33,113].pack('C*')+"\r\n", 0, IP,8899)
end


def green
	SKT.send([34,34,113].pack('C*')+"\r\n", 0, IP,8899)
end


def blue
	SKT.send([34,35,113].pack('C*')+"\r\n", 0, IP,8899)
end

def rgb_pwm(str)
	SKT.send([34,39].pack('C*') + str + "\r\n", 0, IP,8899)
end

def turn_off
	SKT.send([34,33,37].pack('C*')+"\r\n", 0, IP,8899)
	SKT.send([34,34,37].pack('C*')+"\r\n", 0, IP,8899)
	SKT.send([34,35,37].pack('C*')+"\r\n", 0, IP,8899)
end

def animate count, units
	if(units == :times)
		count.times{|i|
			one_run :fwd
			one_run :bck
		}
	else 
		if(units == :seconds)
		end
	end	
end

def one_run(direction)
	reds = [16, 24, 48, 75, 128, 192, 224, 192, 64, 32, 16, 8, 0, 0, 0, 0, 0, 0, 8, 24, 48, 64, 128, 255, 192, 128, 96, 48, 16, 0]
	greens = [0, 48, 128, 192, 224, 192, 128, 48, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	yellows = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 48, 128, 192, 224, 192, 128, 48, 0, 0, 0, 0]

	30.times{|i|
		red, green, yellow = reds[i], greens[i], yellows[i] if direction == :fwd
		red, green, yellow = reds[-i], greens[-i], yellows[-i] if direction == :bck
		
		pwm_str = "#{red} #{green} #{yellow}"
		
		pp "RED GREEN YELLOW: #{pwm_str}"
		
		rgb_pwm pwm_str
		
		sleep 0.5
	}
end
