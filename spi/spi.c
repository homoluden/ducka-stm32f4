#include "spi.h"

void init_SPI2(void){

    GPIO_InitTypeDef GPIO_InitStruct;
    SPI_InitTypeDef SPI_InitStruct;

    // enable clock for used IO pins
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);


    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_10 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOB, &GPIO_InitStruct);

    // connect SPI2 pins to SPI alternate function
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource10, GPIO_AF_SPI2);
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource13, GPIO_AF_SPI2);
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource14, GPIO_AF_SPI2);
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource15, GPIO_AF_SPI2);

    // enable peripheral clock
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);

    /* configure SPI2 in Mode 0
     * CPOL = 0 --> clock is low when idle
     * CPHA = 1 --> data is sampled at the second edge
     */
    SPI_InitStruct.SPI_Direction = SPI_Direction_1Line_Tx; // set to TX only mode
    SPI_InitStruct.SPI_Mode = SPI_Mode_Master;     // transmit in master mode, NSS pin has to be always high
    SPI_InitStruct.SPI_DataSize = SPI_DataSize_8b; // one packet of data is 8 bits wide
    SPI_InitStruct.SPI_CPOL = SPI_CPOL_Low;        // clock is low when idle
    SPI_InitStruct.SPI_CPHA = SPI_CPHA_2Edge;      // data sampled at second edge
    SPI_InitStruct.SPI_NSS = SPI_NSS_Soft | SPI_NSSInternalSoft_Set; // set the NSS management to internal and pull internal NSS low
    SPI_InitStruct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_16; // SPI frequency is APB1 frequency / 16 = 5.25 MHz
    SPI_InitStruct.SPI_FirstBit = SPI_FirstBit_MSB;// data is transmitted MSB first
    SPI_Init(SPI2, &SPI_InitStruct);

    SPI_Cmd(SPI2, ENABLE); // enable SPI2
}

/* This funtion is used to transmit and receive data
 * with SPI2
 *          data --> data to be transmitted
 *          returns received value
 */
void SPI2_send(u8 data){

    SPI_I2S_SendData(SPI2,data);
    //SPI2->DR = data; // write data to be transmitted to the SPI data register
    while( !(SPI2->SR & SPI_I2S_FLAG_TXE) ); // wait until transmit complete

    // No need to wait Receive. Just send...
    //while( !(SPI2->SR & SPI_I2S_FLAG_RXNE) ); // wait until receive complete
    //while( SPI2->SR & SPI_I2S_FLAG_BSY ); // wait until SPI is not busy anymore
    //return SPI2->DR; // return received data from SPI data register
}
