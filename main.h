#ifndef __MAIN_H
#define __MAIN_H

#include <string.h>
#include "stm32f4xx.h"
#include "stm32f4xx_it.h"
#include <stdio.h>
#include "stm32f4xx_usart.h"
#include "stm32_ub_uart.h"
#include <stm32f4xx_gpio.h>
#include <stm32f4xx_rcc.h>

extern uint32_t SystemCoreClock;

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"

#include <math.h>

/* Prototypes ------------------------------------------*/
extern uint32_t SystemCoreClock;

void Delay(__IO uint32_t nCount);
void Gpio_Init(void);
void Usart_Init(void);
void USART_Putc(volatile char c);
void USART_Puts(char* str);
void USART2_IRQHandler(void);
void USART_InsertToBuffer(u8 c);
u8 USART_Getc(void);
u16 USART_Gets(u8* buffer, u16 bufsize);
u8 USART_BufferEmpty(void);

void Initialize_USR();

void vTask_RemoteCommandHandling(void *pvParams);
void Parse_RemoteCommand(u16 cmd, char *buf);

void vTask_UpdateRGB(void *pvParams);

/*------------------------------------------------------*/


/* Defines ---------------------------------------------*/
#define COERCE_DOUBLE(lb, val, rb) round(fmin(fmax(lb, val), rb))

typedef enum {
    // State Commands (numbers should be in range of [RX_FIRST_CHR... RX_LAST_CHR])
    CMD_STREAM_STATE = (u16)((0x21<<8) + 0x21), // State streaming On / Off

    CMD_LED_R = (u16)((34<<8) + 33), // LED1 On / Off
    CMD_LED_G = (u16)((34<<8) + 34), // LED2 On / Off
    CMD_LED_B = (u16)((34<<8) + 35), // LED3 On / Off
//    CMD_PWM_R = (u16)((34<<8) + 36), // Red LED PWM set
//    CMD_PWM_G = (u16)((34<<8) + 37), // Green LED PWM set
//    CMD_PWM_B = (u16)((34<<8) + 38), // Blue LED PWM set
    CMD_PWM_RGB = (u16)((34<<8) + 39), // RGB LED PWMs set. Example for CMD value: "8400 4200 2100"

    // Errors
    CMD_ERR_TRUNC = (u16)((0x7E<<8) + 0x7E), // Buffer was full. Command content probably truncated.

}REMOTE_CMD_t;
typedef enum {
    CMD_OPT_ON = 113,   // Last simple num in RX_..._CHR range
    CMD_OPT_OFF = 37    // First simple num in RX_..._CHR range
}REMOTE_CMD_OPT_t;

typedef struct {
  double A[4];
  double B[2];
  double C[2];
  double D;
  double X[2];
}SS_Model;
/*------------------------------------------------------*/

#endif // __MAIN_H
